import axios from "axios";
const BASE_URL = import.meta.env.VITE_API_BASE_URL;

export const fetchApi = async () => axios.get(BASE_URL);
