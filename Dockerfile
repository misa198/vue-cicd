# build stage
FROM node:18.11.0-alpine3.15 as build-stage
WORKDIR /app
COPY . .
RUN npm install --silent
RUN npm run build

# production stage
FROM nginx:1.23.2-alpine as production-stage
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
